package org.omarsalem.rfg;

import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class ViewTrackerImplTest {
    private ViewTracker target;

    @Before
    public void init() {
        target = new ViewTrackerImpl();
    }

    @After
    public void cleanup() {
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void trackView_stalePage_stalePageRemoved() {
        //Arrange
        final String oldPage = "oldPage";
        final String admin = "admin";
        target.trackView(admin, oldPage);

        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 11000);

        final String newPage = "newPage";
        target.trackView(admin, newPage);

        //Act
        final Collection admins = target.getAdmins(oldPage);

        //Assert
        assertEquals(admins.size(), 0);
    }

    @Test
    public void getAdmins_noTrackedPages_emptyList() {
        //Arrange
        final String pageId = "pageId";

        //Act
        final Collection admins = target.getAdmins(pageId);

        //Assert
        assertEquals(admins.size(), 0);
    }

    @Test
    public void getAdmins_freshPages_adminsReturned() {
        //Arrange
        final String pageId = "pageId";
        final String admin = "admin";
        target.trackView(admin, pageId);

        //Act
        final Collection admins = target.getAdmins(pageId);

        //Assert
        assertEquals(admins.size(), 1);
        assertEquals(new ArrayList<String>(admins).get(0), admin);
    }

    @Test
    public void getAdmins_stalePage_stalePageRemoved() {
        //Arrange
        final String oldPage = "oldPage";
        final String admin = "admin";
        target.trackView(admin, oldPage);
        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 11000);

        //Act
        final Collection admins = target.getAdmins(oldPage);

        //Assert
        assertEquals(admins.size(), 0);
    }

    @Test
    public void getAdmins_staleAndFreshPages_freshPageAdminsReturned() {
        //Arrange
        final String oldPage = "oldPage";
        final String admin = "admin";
        target.trackView(admin, oldPage);

        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 11000);

        final String newPage = "newPage";
        target.trackView(admin, newPage);

        //Act
        final Collection admins = target.getAdmins(newPage);

        //Assert
        assertEquals(admins.size(), 1);
        assertEquals(new ArrayList<String>(admins).get(0), admin);
    }

    @Test
    public void getAdmins_pageViewedTwiceDifferentAdmins_twoAdminsReturned() {
        //Arrange
        final String page = "page";

        final String adminA = "adminA";
        final String adminB = "adminB";
        target.trackView(adminA, page);

        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 5000);
        target.trackView(adminB, page);

        //Act
        final Collection admins = target.getAdmins(page);

        //Assert
        assertEquals(admins.size(), 2);
        final ArrayList<String> adminsArray = new ArrayList<String>(admins);
        assertEquals(adminsArray.get(0), adminA);
        assertEquals(adminsArray.get(1), adminB);
    }

    @Test
    public void getPages_noTrackedPages_emptyList() {
        //Arrange
        final String admin = "admin";

        //Act
        final Collection pages = target.getPages(admin);

        //Assert
        assertEquals(pages.size(), 0);
    }

    @Test
    public void getPages_stalePage_stalePageRemoved() {
        //Arrange
        final String oldPage = "oldPage";
        final String admin = "admin";
        target.trackView(admin, oldPage);
        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 11000);

        //Act
        final Collection pages = target.getPages(admin);

        //Assert
        assertEquals(pages.size(), 0);
    }

    @Test
    public void getPages_staleAndFreshPages_freshPagesReturned() {
        //Arrange
        final String oldPage = "oldPage";
        final String admin = "admin";
        target.trackView(admin, oldPage);

        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 11000);

        final String newPage = "newPage";
        target.trackView(admin, newPage);

        //Act
        final Collection pages = target.getPages(admin);

        //Assert
        assertEquals(pages.size(), 1);
        assertEquals(new ArrayList<Page>(pages).get(0).getId(), newPage);
    }

    @Test
    public void getPages_pageViewedTwiceDifferentAdmins_onePageReturned() {
        //Arrange
        final String page = "page";

        final String adminA = "adminA";
        final String adminB = "adminB";
        target.trackView(adminA, page);

        DateTimeUtils.setCurrentMillisFixed(new Date().getTime() + 5000);
        target.trackView(adminB, page);

        //Act
        Collection pages = target.getPages(adminA);

        //Assert
        assertEquals(pages.size(), 1);

        //Act
        pages = target.getPages(adminB);

        //Assert
        assertEquals(pages.size(), 1);
    }

    @Test
    public void trackView_multipleConcurrentCallsSameAdminSamePage_singleAdminAndPage() {
        //Arrange
        final String page = "page";
        final String admin = "admin";
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(5);

        //Act
        IntStream
                .range(0, 5)
                .forEach(i -> service.submit(() -> {
                    target.trackView(admin, page);
                    latch.countDown();
                }));

        final Collection admins = target.getAdmins(page);
        final Collection pages = target.getPages(admin);


        assertEquals(1, admins.size());
        assertEquals(1, pages.size());

    }

    @Test
    public void trackView_multipleConcurrentCallsSameAdminDiffPage_singleAdminMultiplePages() throws InterruptedException {
        //Arrange
        final String page = "page";
        final String admin = "admin";
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(5);

        //Act
        IntStream
                .range(1, 6)
                .forEach(i -> service.submit(() -> {
                    target.trackView(admin, page + i);
                    latch.countDown();
                }));
        latch.await();

        final Collection pages = target.getPages(admin);


        assertEquals(5, pages.size());
        for (int i = 1; i < 6; i++) {
            assertEquals(1, target.getAdmins(page + i).size());
        }

    }
}