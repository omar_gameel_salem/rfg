package org.omarsalem.rfg;

import org.joda.time.DateTimeUtils;

import java.util.Date;
import java.util.Objects;

public class Page implements Comparable<Page> {

    private final Date lastViewed = new Date(DateTimeUtils.currentTimeMillis());
    private final String id;

    public Page(String id) {
        this.id = id;
    }

    public Date getLastViewed() {
        return lastViewed;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "{" +
                "lastViewed=" + lastViewed +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public int compareTo(Page o) {
        return getLastViewed().compareTo(o.getLastViewed());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return Objects.equals(id, page.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
