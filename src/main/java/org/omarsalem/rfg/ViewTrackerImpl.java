package org.omarsalem.rfg;

import org.joda.time.DateTimeUtils;

import java.util.*;

public class ViewTrackerImpl implements ViewTracker {
    private static final int TIMEOUT_SECONDS = 10;
    private final Queue<Page> freshPages = new PriorityQueue<>();
    private final Map<Page, Set<String>> pagesMap = new HashMap<>();
    private final Map<String, Set<Page>> adminsMap = new HashMap<>();

    /**
     *
     * O(nlog(n))
     * @param admin the username of the admin viewing the page
     * @param pageId the id of the page
     */
    @Override
    public synchronized void trackView(String admin, String pageId) {
        final Page page = new Page(pageId);
        refreshPages(page);
        freshPages.add(page);
        addToPagesMap(admin, page);
        addToAdminsMap(admin, page);
    }

    /**
     *
     * O(nlog(n))
     * @param pageId the id of the page
     * @return
     */
    @Override
    public synchronized Collection getAdmins(String pageId) {
        refreshPages();
        final Page key = new Page(pageId);
        if (!pagesMap.containsKey(key)) {
            return new ArrayList();
        }
        return pagesMap.get(key);
    }

    /**
     *
     * O(nlog(n))
     * @param admin the username of the admin
     * @return
     */
    @Override
    public synchronized Collection getPages(String admin) {
        refreshPages();
        if (!adminsMap.containsKey(admin)) {
            return new ArrayList();
        }
        return adminsMap.get(admin);
    }

    private void refreshPages(Page page) {
        freshPages.remove(page);
        refreshPages();
    }

    private void refreshPages() {
        while (!freshPages.isEmpty() && getDateDiffSeconds(freshPages.peek()) > TIMEOUT_SECONDS) {
            final Page removedPage = freshPages.poll();
            final Set<String> admins = pagesMap.get(removedPage);
            admins.forEach(adminsMap::remove);
            pagesMap.remove(removedPage);
        }
    }

    private long getDateDiffSeconds(Page oldestPage) {
        final long l = DateTimeUtils.currentTimeMillis() - oldestPage.getLastViewed().getTime();
        return l / 1000;
    }

    private void addToPagesMap(String admin, Page page) {
        if (!pagesMap.containsKey(page)) {
            pagesMap.put(page, new HashSet<>());
        }
        pagesMap.get(page).add(admin);
    }

    private void addToAdminsMap(String admin, Page page) {
        if (!adminsMap.containsKey(admin)) {
            adminsMap.put(admin, new HashSet<>());
        }
        adminsMap.get(admin).add(page);
    }
}
