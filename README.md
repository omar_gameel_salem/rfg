# README #

### Coding Exercise ###

In our application, we need to track in real time which administrators are viewing which pages. To do this, each page has a small piece of javascript that sends an asynchronous http command every 5 seconds to our server to notify the server that the page is still open in a browser. If more than 10 seconds elapses without receiving this command, we can assume that the page is closed.

On the server, we track all of this using an in-memory data structure called a ViewTracker. Every 5 seconds the trackView method is called with the name of an admin and a pageId. There are two other methods for getting the pages that any admin is viewing and also which admins are viewing any page at any given moment. Attached to this email, you can find the java interface that describes the data structure.

Please create an implementation of this data structure. You may use any classes in the java.util.* packages or any other resources you think would be beneficial. There is not one right answer. We're interested in seeing what approach you take, as well as the quality of the code you write. Note that the two getter methods in the interface return any type of Collection. We have not specified what kind of collection they should be or even what type of objects should be in the collection. These are implementation details. Please make your own design choice to create a data structure that will be as efficient as possible. Also keep in mind the requirement that this data structure must be thread-safe.

```
import java.util.Collection;

/**
 * Code Interview Challenge
 *
 * Create a Data Structure for simultaneously tracking what pages are currently being 
 * viewed by each admin, and which admins are viewing each page.
 *
 * Pages will notify the ViewTracker every 5 seconds that they are still being viewed. 
 * If more than 10 seconds passes without a call to trackView, a page should be considered closed.
 *
 * The implementation should be
 * 1. Thread-safe (different threads will call trackView and many threads will simultaneously call getPages or getAdmins)
 * 2. As fast as possible. These methods will be called very frequently
 * 3. Not leak memory. Once a page view is more than 10 seconds old it may be discarded.
 */
public interface ViewTracker {

	/**
	* This method is called to indicate that a page is currently being viewed by an admin. The web page typically has
	* a JavaScript timer which notifies the server to call this method every 5 seconds. If more than 10 seconds pass without 
	* receiving the method call, the ViewTracker may assume that the browser is no longer displaying the page.
	* @param admin the username of the admin viewing the page
	* @param pageId the id of the page
	*/
	public void trackView(String admin, String pageId);

	/**
	*
	* @param pageId the id of the page
	* @return a collection of the admins currently viewing this page
	*/
	public Collection getAdmins(String pageId);

	/**
	*
	* @param admin the username of the admin
	* @return a collection of the pages currently being viewed by this admin
	*/
	public Collection getPages(String admin);

}
```

### Assumptions###
number of unique pages is finite and fits in memory (not in billions for example)
otherwise heap and maps' size should be capped

### Requirements to build and run the solution###

* Java 8
* Maven

```
cd rfg
mvn clean surefire-report:report
```
report will be at /target/site/surefire-report.html